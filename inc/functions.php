<?php

function PrintMenu()
{
    
    echo '<li><a href="index.php">Bemutatkozás</a></li>
                <li><a href="index.php?page=services">Szolgáltatásaink</a></li>
                <li><a href="index.php?page=articles">Cikkek</a></li>
                <li><a href="index.php?page=gallery">Galéria</a></li>
                <li><a href="index.php?page=contact">Kapcsolat</a></li>';
    
}

function DBConn()
{
    $dsn = "mysql:host=localhost;dbname=alcoholwebshop;charset=utf8mb4";
    $user = "root";
    $pass = "mysql";
    
    $db = new PDO($dsn, $user, $pass);
    
    return $db; 
}

function createUser($username,$passwd,$email,$birthdate){
  
try {
   $dbh = DBConn();
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

// Then you can prepare a statement and execute it.    
$stmt = $dbh->prepare("CALL create_user(?, ?, ?, ?)");
// One bindParam() call per parameter
$stmt->bindParam(1, $username, PDO::PARAM_STR,1000); 
$stmt->bindParam(2, $passwd, PDO::PARAM_STR,1000); 
$stmt->bindParam(3, $email, PDO::PARAM_STR,256); 
$stmt->bindParam(4, $birthdate, PDO::PARAM_STR); 

// call the stored procedure
$stmt->execute();
   
}

function createBillingAddress($fullnameIN,$phoneIN,$houseIN,$cityIN,$streetIN,$zipcodeIN,$RegionIN){
  
try {
   $dbh = DBConn();
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

// Then you can prepare a statement and execute it.    
$stmt = $dbh->prepare("CALL create_billing_address(:fullnameIN, :phoneIN, :houseIN, :cityIN, :streetIN, :zipcodeIN, :RegionIN, @idOUT)");
// One bindParam() call per parameter
$stmt->bindParam(':fullnameIN', $fullnameIN, PDO::PARAM_STR,45); 
$stmt->bindParam(':phoneIN', $phoneIN, PDO::PARAM_STR,11); 
$stmt->bindParam(':houseIN', $houseIN, PDO::PARAM_STR,45); 
$stmt->bindParam(':cityIN', $cityIN, PDO::PARAM_STR,45); 
$stmt->bindParam(':streetIN', $streetIN, PDO::PARAM_STR,45); 
$stmt->bindParam(':zipcodeIN', $zipcodeIN, PDO::PARAM_INT); 
$stmt->bindParam(':RegionIN', $RegionIN, PDO::PARAM_STR,45); 
 
// call the stored procedure
$stmt->execute();
    
$row = $dbh->query("select @idOUT;")->fetch(PDO::FETCH_ASSOC);;

$idOUT= (int) $row['@idOUT'];
    
return $idOUT;
    
}

function createOrder($billingIN,$paymentIN)
{
    
try {
   $dbh = DBConn();
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}
$idOUT=0;
// Then you can prepare a statement and execute it.    
$stmt = $dbh->prepare("CALL create_order(:billingIN,:paymentIN,@idOUT)");
// One bindParam() call per parameter
$stmt->bindParam(':billingIN', $billingIN, PDO::PARAM_INT); 
$stmt->bindParam(':paymentIN', $paymentIN, PDO::PARAM_INT); 


// call the stored procedure
$stmt->execute();
    
$row = $dbh->query("select @idOUT;")->fetch(PDO::FETCH_ASSOC);;

$idOUT= (int) $row['@idOUT'];
    
return $idOUT;
}

function setOrderProduct($OrderidIN,$ProductIN,$Quantity){
  
try {
   $dbh = DBConn();
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

// Then you can prepare a statement and execute it.    
$stmt = $dbh->prepare("CALL set_order_product(?, ?, ?)");
// One bindParam() call per parameter
$stmt->bindParam(1, $OrderidIN, PDO::PARAM_INT); 
$stmt->bindParam(2, $ProductIN, PDO::PARAM_STR,45); 
$stmt->bindParam(3, $Quantity, PDO::PARAM_INT); 

// call the stored procedure
$stmt->execute();
    
}

function createRating($nameIN,$ratingIN,$commentIN,$ProductidIN){
  
try {
   $dbh = DBConn();
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

// Then you can prepare a statement and execute it.    
$stmt = $dbh->prepare("CALL create_rating(?, ?, ?, ?)");
// One bindParam() call per parameter
$stmt->bindParam(1, $nameIN, PDO::PARAM_STR,45); 
$stmt->bindParam(2, $ratingIN, PDO::PARAM_STR,45); 
$stmt->bindParam(3, $commentIN, PDO::PARAM_STR,65635); 
$stmt->bindParam(4, $ProductidIN, PDO::PARAM_INT); 

// call the stored procedure
$stmt->execute();
    
}

function getUserPAss($nameIN){
  
try {
   $dbh = DBConn();
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

// Then you can prepare a statement and execute it.    
$stmt = $dbh->prepare("CALL get_user_pass_by_name(:nameIN,@passwdOUT)");
// One bindParam() call per parameter
$stmt->bindParam(':nameIN', $nameIN, PDO::PARAM_STR,45); 


// call the stored procedure
$stmt->execute();
$row = $dbh->query("select @passwdOUT;")->fetch(PDO::FETCH_ASSOC);;

$passwdOUT= $row['@passwdOUT'];
    
return $passwdOUT;
    
}

function setUserBillingAddress($useridIN,$billingIN){
  
try {
   $dbh = DBConn();
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

// Then you can prepare a statement and execute it.    
$stmt = $dbh->prepare("CALL set_user_billing_address(?, ?)");
// One bindParam() call per parameter
$stmt->bindParam(1, $useridIN, PDO::PARAM_INT); 
$stmt->bindParam(2, $billingIN, PDO::PARAM_INT); 


// call the stored procedure
$stmt->execute();
    
}

function updateUserPasswd($usernameIN,$passwdIN){
  
try {
   $dbh = DBConn();
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

// Then you can prepare a statement and execute it.    
$stmt = $dbh->prepare("CALL update_user_passwd(?, ?)");
// One bindParam() call per parameter
$stmt->bindParam(1, $usernameIN, PDO::PARAM_STR,1000); 
$stmt->bindParam(2, $passwdIN, PDO::PARAM_STR,1000); 


// call the stored procedure
$stmt->execute();
    
}

function getUserEmail($nameIN) {
    try {
   $dbh = DBConn();
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

// Then you can prepare a statement and execute it.    
$stmt = $dbh->prepare("CALL get_user_email(:nameIN,@emailOUT)");
// One bindParam() call per parameter
$stmt->bindParam(':nameIN', $nameIN, PDO::PARAM_STR,45); 


// call the stored procedure
$stmt->execute();
$row = $dbh->query("select @emailOUT;")->fetch(PDO::FETCH_ASSOC);;


$emailOUT= $row['@emailOUT'];
  
return $emailOUT;
        
}
function getUserBirth($nameIN) {
    try {
   $dbh = DBConn();
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

// Then you can prepare a statement and execute it.    
$stmt = $dbh->prepare("CALL get_user_birth(:nameIN,@birthOUT)");
// One bindParam() call per parameter
$stmt->bindParam(':nameIN', $nameIN, PDO::PARAM_STR,45); 


// call the stored procedure
$stmt->execute();
$row = $dbh->query("select @birthOUT;")->fetch(PDO::FETCH_ASSOC);;


$birthOUT= $row['@birthOUT'];

return $birthOUT;      
}

function getUserID($nameIN) {
    try {
   $dbh = DBConn();
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

// Then you can prepare a statement and execute it.    
$stmt = $dbh->prepare("CALL get_user_id_by_name(:nameIN,@idOUT)");
// One bindParam() call per parameter
$stmt->bindParam(':nameIN', $nameIN, PDO::PARAM_STR,45); 


// call the stored procedure
$stmt->execute();
$row = $dbh->query("select @idOUT;")->fetch(PDO::FETCH_ASSOC);;


$birthOUT= $row['@idOUT'];

return $birthOUT;      
}

function getBillingAddressID($nameIN) {
    try {
   $dbh = DBConn();
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

// Then you can prepare a statement and execute it.    
$stmt = $dbh->prepare("CALL get_user_billing_address_id(:nameIN,@idOUT)");
// One bindParam() call per parameter
$stmt->bindParam(':nameIN', $nameIN, PDO::PARAM_STR,45); 


// call the stored procedure
$stmt->execute();
$row = $dbh->query("select @idOUT;")->fetch(PDO::FETCH_ASSOC);;


$birthOUT= $row['@idOUT'];

return $birthOUT;      
}



function getUserIDbyEMAIL($emailIN) {
    try {
   $dbh = DBConn();
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

// Then you can prepare a statement and execute it.    
$stmt = $dbh->prepare("CALL get_user_id_by_email(:emailIN,@idOUT)");
// One bindParam() call per parameter
$stmt->bindParam(':emailIN', $emailIN, PDO::PARAM_STR,45); 


// call the stored procedure
$stmt->execute();
$row = $dbh->query("select @idOUT;")->fetch(PDO::FETCH_ASSOC);;


$idOUT= $row['@idOUT'];

return $idOUT;      
}


//login logout cookie

function logout(){ 
//logout.php
setcookie("LoggedinUser", "", time()-3600,"/");

}


function loggedinCookie($cookie_value){
//login.php
$cookie_name = "LoggedinUser";
setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");


}

function getBillingInfo($billidIN){
  
try {
   $dbh = DBConn();
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

// Then you can prepare a statement and execute it.    
$stmt = $dbh->prepare("CALL get_bill_info(:billidIN,@nameOUT, @phoneOUT, @regionOUT, @cityOUT, @zipcodeOUT, @streetOUT, @numberOUT)");
// One bindParam() call per parameter
$stmt->bindParam(':billidIN', $billidIN, PDO::PARAM_INT); 
 
// call the stored procedure
$stmt->execute();
    
$row = $dbh->query("select @nameOUT,@phoneOUT,@regionOUT,@cityOUT,@zipcodeOUT,@streetOUT,@numberOUT;")->fetch(PDO::FETCH_ASSOC);;


    
return $row;
    
}




?>