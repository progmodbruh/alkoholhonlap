var fill = document.getElementsByClassName("autofill");
var fillbutton = document.getElementById("samebilling");

fillbutton.addEventListener("click", autofill);

function autofill(){
    
    if(fillbutton.checked){
        for(var i = 0; i < fill.length / 2; i++){
            fill[i+8].value = fill[i].value;
        }     
    }else{
        for(var i = 0; i < fill.length / 2; i++){
            fill[i+8].value = "";
        }  
    }
    updateBtn();
}

var orderbutton = document.getElementById("orderBtn");
orderbutton.addEventListener("mouseenter", function(){
    updateBtn();
});


checkFilled();
function checkFilled(){
    for(var i = 0; i < fill.length; i++){
        fill[i].addEventListener("change", function(){
            updateBtn();
        })
    }
}

var select = document.getElementsByClassName("select-css");
    for(var i = 0; i < select.length; i++){
        select[i].addEventListener("change", function(){
        updateBtn();    
    })
}

function updateBtn(){
    var filled = true;
    
    for(var i = 0; i < fill.length; i++){
        if(fill[i].value == "")
            filled = false; 
    }
    for(var i = 0; i < select.length; i++)
        if(select[i].selectedIndex == 0)
            filled = false;
    
    if(filled == false)
        orderbutton.disabled = true;
    else
        orderbutton.disabled = false;
    
}

