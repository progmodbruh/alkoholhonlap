
//console.log('sdfsdf');
var updateButtons = document.getElementsByClassName('add-to-cart');
var cartmessage = document.getElementById("addedtocart");

for(var i = 0; i < updateButtons.length; i++){
    updateButtons[i].addEventListener('click', function(){
        var productID = this.dataset.product;
        var name = this.dataset.name;    
        var price = this.dataset.price;
        var action = this.dataset.action;  
        var quantity = 1;
        userOrder(productID, action, quantity, name, price);
        anim();
    })
}

//add to cart animáció
function anim(){
    setTimeout(toBlank, 1500);
    cartmessage.style.animationName = "added";
}
function toBlank(){
    cartmessage.style.animationName = "blank";
}


//rendelés gombra kattintás
function userOrder(productID, action, quantity, name, price){
    
    var alreadyin = JSON.parse(getCookie("cart"));
    var cookievalue = parseInt( getCookie("total"));
    console.log("colkie értéke: " + cookievalue);
    console.log("ár: " + price);
    var totalprice = cookievalue += parseInt(price);
    console.log("teljes ár: " + totalprice);
    document.cookie ="total=" + totalprice + ";domain=;path=/";

    for(var i = 0; i < alreadyin.length; i++) {
        var obj = alreadyin[i];
        if(obj.productID == productID){
            action = "update";
            alreadyin[i].quantity++;
        }            
    }
    if(action == "update")
        document.cookie ='cart=' + JSON.stringify(alreadyin) + ";domain=;path=/";  
          
    if(action == "add"){

        var content = [{
            "productID" : productID,
            "name" : name,
            "price" : price,
            "quantity" : quantity
        }];
    
        cart = cart.concat(content);
        document.cookie ='cart=' + JSON.stringify(cart) + ";domain=;path=/";    
        
    }

}

//HA A CART OLDALON VAGYUNK
//cart oldal kosár html tartalma

if(document.getElementById('cart-table') != null){
    
    var totalprice = 0;
    var alreadyin = JSON.parse(getCookie("cart"));
    for(var i = 0; i < alreadyin.length; i++){
        document.getElementById('cart-table').innerHTML += '<tr><td class="productnames">'+ alreadyin[i].name +'</td><td class="productprices">'+ alreadyin[i].price +' Ft</td><td><input type="number" name="targy" class="productAmount" maxlength="254" value="'+ alreadyin[i].quantity +'" min="1" autocomplete=off></td><td class="totalprices">'+ alreadyin[i].quantity * alreadyin[i].price +' Ft</td></tr>';  
        totalprice += parseInt(alreadyin[i].price*alreadyin[i].quantity);
    }
    
    totalPriceUpdate();
    
    var empty = document.getElementById("empty-cart");
    empty.addEventListener("click", emptyCart);
    
    document.cookie ='total=' + totalprice + ";domain=;path=/"; 

    var amounts = document.getElementsByClassName("productAmount");

    for(var i = 0; i < amounts.length; i++){
        amounts[i].addEventListener('click', priceUpdate);
        amounts[i].addEventListener('keyup', priceUpdate);
        amounts[i].addEventListener('keydown', priceUpdate);
    }
}


function priceUpdate(){
    var names = document.getElementsByClassName("productnames");
    var prices = document.getElementsByClassName("productprices");
    var amounts = document.getElementsByClassName("productAmount");
    var totalprices = document.getElementsByClassName("totalprices");
    
    for(var i = 0; i < names.length; i++){
           
        totalprices[i].innerHTML = (prices[i].innerHTML.replace(/\D/g, "")*amounts[i].value).toString() + " Ft";
        totalPriceUpdate();
    }
    
    var cart = JSON.parse(getCookie("cart"));
    console.log("eredeti cart: ", cart);
    for(var i = 0; i < cart.length; i++){        
        cart[i].quantity = amounts[i].value;
    }
    console.log("updatelt cart: ", cart);
    document.cookie ='cart=' + JSON.stringify(cart) + ";domain=;path=/";   
    totalPriceUpdate();
    
    var totalprice = 0;
    var alreadyin = JSON.parse(getCookie("cart"));
    for(var i = 0; i < alreadyin.length; i++){
        totalprice += parseInt(alreadyin[i].price*alreadyin[i].quantity);
    }
    document.cookie ='total=' + totalprice + ";domain=;path=/"; 

}

//összár cookie+html update
function totalPriceUpdate(){
    var totalprice = 0;
    var totalprices = document.getElementsByClassName("totalprices");
    for(var i = 0; i < totalprices.length; i++){
        totalprice += parseInt(totalprices[i].innerHTML);
    }
    if(totalprices.length == 0)
        document.getElementById("totalprice").innerHTML = "Your cart is empty!";
    else
        document.getElementById("totalprice").innerHTML = "Total price: " + totalprice.toString() + " Ft";
}

document.getElementById("finalprice").innerHTML = "total: " + getCookie("total") + " Ft";


//kiürítés
function emptyCart(){
    cart = [];
    document.cookie ='cart=' + JSON.stringify(cart) + ";domain=;path=/";
    location.reload;
    
}
