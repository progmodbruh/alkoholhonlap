<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link rel="stylesheet" href="res/css/style.css?v=<?php echo time(); ?>">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="js/order.js"></script>
    <script defer src="js/cart.js"></script>
    <script defer src="js/autofill.js"></script>
    <script defer src="js/scrolltotop.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
    <script type="text/javascript">
    

		function getCookie(name) {
		    var cookieArr = document.cookie.split(";");

		    for(var i = 0; i < cookieArr.length; i++) {
		        var cookiePair = cookieArr[i].split("=");

		        if(name == cookiePair[0].trim()) {
		            return decodeURIComponent(cookiePair[1]);
		        }
		    }
		    return null;
		}


		function uuidv4() {
		  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
		    return v.toString(16);
		  });
		}


		var user = getCookie('user');

		if (user == null || user == undefined){
			user = uuidv4();
		}

		document.cookie ='user=' + user + ";domain=;path=/";

        var cart = JSON.parse(getCookie('cart'));

		if (cart == undefined){
			cart = [];
			document.cookie ='cart=' + JSON.stringify(cart) + ";domain=;path=/";
		}

    </script>

</head>

<body>
    <img id="breweryImg" src="res/graphics/background/brewery.svg" alt="">
    <!-- <script src=""></script> -->

    <header>
        <div id="cim">

            <h1 class="felsocim">Handcrafted Beer</h1>
            <h1 class="alsocim">Simply the best</h1>
            <img class="imgcim" src="res/graphics/icons/beercaps.svg" alt="">
            <div class="topbuttons">
            <?php
if(!isset($_COOKIE["LoggedinUser"])) {
    echo '<a href="index.php?page=login" class="buttonTop">Login</a>';

} else {
     echo '<a href="index.php?page=logout" class="buttonTop">Logout</a>';
     echo '<a href="index.php?page=profile" class="buttonTop">Profile</a>';
}
?>

            <a href="index.php?page=cart" class="buttonTop cartBtn">🛒</a>
                </div>

        </div>
        <nav>
            <ul>
                <li><a href="index.php">Home</a></li>
                <li><a href="index.php?page=sorok">Products</a></li>
                <li><a href="index.php?page=gallery">Gallery</a></li>
                <li><a href="index.php?page=velemeny">Review</a></li>
                <li><a href="index.php?page=contact">Contact</a></li>
            </ul>
        </nav>

    </header>
