<footer>
        <h1 id="footerTitle">Contact Us</h1>
        <img class="imgcim" src="res/graphics/icons/beercaps_white.svg" alt="">
        <div class="footercolumn">
        <div class="column">
        <h2>Send a message</h2>
        
        <form method="post" action="">
                        <p><input class="kapcsolat" type="email" name="email" id="inputEmail" placeholder="E-mail" maxlength="254" value="" required></p>
                        <p><input class="kapcsolat" type="text" name="targy" id="inputTargy" placeholder="Topic" maxlength="254" value="" required></p>
                        <p><textarea class="kapcsolat" id="inputUzenet" name="uzenet" placeholder="Message" rows="10" cols="50" required></textarea></p>
                        <p><a href="" class="buttonFooter">Send</a></p>
                    </form>
</div>
         <div class="column">
         <section id="footerkapcsolat">
          <h3>cserkutisor@gmail.com</h3>
          <h3>+36 30 123 4567</h3>
          <h3>7673 Cserkút, Alkotmány u. 25.</h3>
           </section>
            </div>
<div class="column">
         <section id="footermap">
          <h2>Map</h2>
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5535.5919976114465!2d18.130151249601163!3d46.07510873174865!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4742ae799c56516b%3A0x4b3a4709998bc484!2zQ3NlcmvDunQsIEFsa290bcOhbnkgdS4gMjUsIDc2NzM!5e0!3m2!1shu!2shu!4v1615553665586!5m2!1shu!2shu" width="400" height="300" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
          <p><a href="https://goo.gl/maps/GFasrpkNReLG6wxW9" class="buttonFooter">Google Maps</a></p>
          </section>
           </div>
           </div>
</footer>

<button onclick="topFunction()" id="ScrollToTopBtn" title="Go to top"><img src="res/graphics/icons/uparrow.svg"/ id="ScrollImg"></button>

</body>
</html>