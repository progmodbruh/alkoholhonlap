<?php
function DBConn()
{
    $dsn = "mysql:host=localhost;dbname=alcoholwebshop;charset=utf8mb4";
    $user = "root";
    $pass = "mysql";
    
    $db = new PDO($dsn, $user, $pass);
    
    return $db; 
}

function createBillingAddress($fullnameIN,$phoneIN,$houseIN,$cityIN,$streetIN,$zipcodeIN,$RegionIN){
  
try {
   $dbh = DBConn();
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

// Then you can prepare a statement and execute it.    
$stmt = $dbh->prepare("CALL create_billing_address(:fullnameIN, :phoneIN, :houseIN, :cityIN, :streetIN, :zipcodeIN, :RegionIN, @idOUT)");
// One bindParam() call per parameter
$stmt->bindParam(':fullnameIN', $fullnameIN, PDO::PARAM_STR,45); 
$stmt->bindParam(':phoneIN', $phoneIN, PDO::PARAM_STR,11); 
$stmt->bindParam(':houseIN', $houseIN, PDO::PARAM_STR,45); 
$stmt->bindParam(':cityIN', $cityIN, PDO::PARAM_STR,45); 
$stmt->bindParam(':streetIN', $streetIN, PDO::PARAM_STR,45); 
$stmt->bindParam(':zipcodeIN', $zipcodeIN, PDO::PARAM_INT); 
$stmt->bindParam(':RegionIN', $RegionIN, PDO::PARAM_STR,45); 
 
// call the stored procedure
$stmt->execute();
    
$row = $dbh->query("select @idOUT;")->fetch(PDO::FETCH_ASSOC);;

$idOUT= (int) $row['@idOUT'];
    
return $idOUT;
    
}
function createOrder($billingIN,$paymentIN)
{
    
try {
   $dbh = DBConn();
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}
$idOUT=0;
// Then you can prepare a statement and execute it.    
$stmt = $dbh->prepare("CALL create_order(:billingIN,:paymentIN,@idOUT)");
// One bindParam() call per parameter
$stmt->bindParam(':billingIN', $billingIN, PDO::PARAM_INT); 
$stmt->bindParam(':paymentIN', $paymentIN, PDO::PARAM_INT); 


// call the stored procedure
$stmt->execute();
    
$row = $dbh->query("select @idOUT;")->fetch(PDO::FETCH_ASSOC);;

$idOUT= (int) $row['@idOUT'];
    
return $idOUT;
}
function setOrderProduct($OrderidIN,$ProductIN,$Quantity){
  
try {
   $dbh = DBConn();
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

// Then you can prepare a statement and execute it.    
$stmt = $dbh->prepare("CALL set_order_product(?, ?, ?)");
// One bindParam() call per parameter
$stmt->bindParam(1, $OrderidIN, PDO::PARAM_INT); 
$stmt->bindParam(2, $ProductIN, PDO::PARAM_STR,45); 
$stmt->bindParam(3, $Quantity, PDO::PARAM_INT); 

// call the stored procedure
$stmt->execute();
    
}
function getUserEmail($nameIN) {
    try {
   $dbh = DBConn();
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

// Then you can prepare a statement and execute it.    
$stmt = $dbh->prepare("CALL get_user_email(:nameIN,@emailOUT)");
// One bindParam() call per parameter
$stmt->bindParam(':nameIN', $nameIN, PDO::PARAM_STR,45); 


// call the stored procedure
$stmt->execute();
$row = $dbh->query("select @emailOUT;")->fetch(PDO::FETCH_ASSOC);;


$emailOUT= $row['@emailOUT'];
  
return $emailOUT;
        
}
function getBillingAddressID($nameIN) {
    try {
   $dbh = DBConn();
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

// Then you can prepare a statement and execute it.    
$stmt = $dbh->prepare("CALL get_user_billing_address_id(:nameIN,@idOUT)");
// One bindParam() call per parameter
$stmt->bindParam(':nameIN', $nameIN, PDO::PARAM_STR,45); 


// call the stored procedure
$stmt->execute();
$row = $dbh->query("select @idOUT;")->fetch(PDO::FETCH_ASSOC);;


$birthOUT= $row['@idOUT'];

return $birthOUT;      
}
function getBillingInfo($billidIN){
  
try {
   $dbh = DBConn();
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

// Then you can prepare a statement and execute it.    
$stmt = $dbh->prepare("CALL get_bill_info(:billidIN,@nameOUT, @phoneOUT, @regionOUT, @cityOUT, @zipcodeOUT, @streetOUT, @numberOUT)");
// One bindParam() call per parameter
$stmt->bindParam(':billidIN', $billidIN, PDO::PARAM_INT); 
 
// call the stored procedure
$stmt->execute();
    
$row = $dbh->query("select @nameOUT,@phoneOUT,@regionOUT,@cityOUT,@zipcodeOUT,@streetOUT,@numberOUT;")->fetch(PDO::FETCH_ASSOC);;


    
return $row;
    
}

?>





<?php


$task = $_POST['task'];
$name= $_POST['nev'];
$email= $_POST['email'];
$telefon = $_POST['telefon'];
$region = $_POST['region'];
$city= $_POST['city'];
$zipcode= $_POST['zipcode'];
$street= $_POST['street'];
$number= $_POST['number'];
$payment= $_POST['payment'];
    
  


if ($task == "createOrder"){
    
$username=$_COOKIE["LoggedinUser"];
$id=getBillingAddressID($username);   
$data = getBillingInfo($id);    

$fullnameIN = $data["@nameOUT"];
$emailIN=getUserEmail($username);
$phoneIN = $data["@phoneOUT"];
$RegionIN = $data["@regionOUT"];
$cityIN = $data["@cityOUT"];
$zipcodeIN = $data["@zipcodeOUT"];
$streetIN = $data["@streetOUT"];
$houseIN = $data["@numberOUT"];
    
    if($fullnameIN==$name && $emailIN==$email && $telefon==$phoneIN && $region==$RegionIN && $city==$cityIN && $zipcode==$zipcodeIN && $streetIN==$street && $number==$houseIN){
        
    $orderid=createOrder($id,$payment); 
    }
    else{
        
        $newid=createBillingAddress($name,$telefon,$number,$city,$street,$zipcode,$region);
        $orderid=createOrder($newid,$payment);
    }
    
    
    $cart = stripslashes($_COOKIE['cart']);
    $cartitems = json_decode($cart,true);

    foreach($cartitems as $values) {
    $productID = intval($values["productID"]);
    $quantity = $values["quantity"];
    setOrderProduct($orderid,$productID,$quantity);
    
    
    
    
    
} 

//ide jön a set order product 
    
   
}
    
    
?>