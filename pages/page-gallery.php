<section id="Gallery">
   <h1 id="galleryTitle">Gallery</h1>
   <img class="imgcim" src="res/graphics/icons/beercaps_white.svg" alt="">
   <div class="mainimages">
    <a data-fancybox="gallery" href="res\graphics\gallery\1.jpg"><img src="res\graphics\gallery\1.jpg"></a>
    <a data-fancybox="gallery" href="res\graphics\gallery\2.jpg"><img src="res\graphics\gallery\2.jpg"></a>
    <a data-fancybox="gallery" href="res\graphics\gallery\3.jpg"><img src="res\graphics\gallery\3.jpg"></a>
    <a data-fancybox="gallery" href="res\graphics\gallery\4.jpg"><img src="res\graphics\gallery\4.jpg"></a>
    <a data-fancybox="gallery" href="res\graphics\gallery\5.jpg"><img src="res\graphics\gallery\5.jpg"></a>
    <a data-fancybox="gallery" href="res\graphics\gallery\6.jpg"><img src="res\graphics\gallery\6.jpg"></a>
    <a data-fancybox="gallery" href="res\graphics\gallery\7.jpg"><img src="res\graphics\gallery\7.jpg"></a>
    <a data-fancybox="gallery" href="res\graphics\gallery\8.jpg"><img src="res\graphics\gallery\8.jpg"></a>
    </div>
</section>
