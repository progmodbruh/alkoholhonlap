<img id="breweryImg2" src="res/graphics/background/brewery2.svg" alt="">
<img id="conversationImg" src="res/graphics/background/conversation.svg" alt="">
<section id="mainpage">
<section id="slideshow">
 <div class="slideshowcontainer">
  <img id="slideshowBG" src="res/img/slideshowBG.png">
  <div class="centered"><h1 id="slideshowTitle">The best alcohol in town</h1>
  <h3>Unmatched selection of alcoholic goods</h3></div>
</div>
</section>

<section id="products">
   <h1 id="productsTitle">Our products</h1>
   <img class="imgcim" src="res/graphics/icons/beercaps.svg" alt="">
   <div class="mainDrinks">
    <img src="res/img/Mask Group 2@2x.png" alt="lemon">
    <img src="res/img/Mask Group 3@2x.png" alt="blueberry">
    <img src="res/img/Mask Group 4@2x.png" alt="griotte">
    </div>

    <a href="index.php?page=sorok" class="buttonMain">More</a>

</section>

<section id="Gallery">
   <h1 id="galleryTitle" >Gallery</h1>
   <img class="imgcim" src="res/graphics/icons/beercaps_white.svg" alt="">
   <div class="mainimages">
    <a data-fancybox="gallery" href="res\graphics\gallery\1.jpg"><img src="res\graphics\gallery\1.jpg"></a>
    <a data-fancybox="gallery" href="res\graphics\gallery\2.jpg"><img src="res\graphics\gallery\2.jpg"></a>
    <a data-fancybox="gallery" href="res\graphics\gallery\3.jpg"><img src="res\graphics\gallery\3.jpg"></a>
    <a data-fancybox="gallery" href="res\graphics\gallery\4.jpg"><img src="res\graphics\gallery\4.jpg"></a>
    <a data-fancybox="gallery" href="res\graphics\gallery\5.jpg"><img src="res\graphics\gallery\5.jpg"></a>
    <a data-fancybox="gallery" href="res\graphics\gallery\6.jpg"><img src="res\graphics\gallery\6.jpg"></a>
    <a data-fancybox="gallery" href="res\graphics\gallery\7.jpg"><img src="res\graphics\gallery\7.jpg"></a>
    <a data-fancybox="gallery" href="res\graphics\gallery\8.jpg"><img src="res\graphics\gallery\8.jpg"></a>
    </div>

    <a href="index.php?page=gallery" class="buttonMain">Show more</a>
</section>

<section id="Reviews">
    <h1 id="reviewsTitle">Reviews</h1>
    <img class="imgcim" src="res/graphics/icons/beercaps_green.svg" alt="">
    <a href="index.php?page=velemeny" class="buttonMain">Write a review</a>
</section>
</section>
