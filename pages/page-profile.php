<div class="profilediv">
  <section id="profile">

    <h1 class="redtitle">Profile Information</h1>

<?php
    $username=$_COOKIE["LoggedinUser"];
    $email=getUserEmail($username);
    $birthdate=getUserBirth($username);


      echo '<h2>'.$username.'</h2>
    <h2>'.$email.'</h2>
    <h2>'.$birthdate.'</h2>'

?>


</section>

<section id="profileshipping">
  <h1 class="redtitle">Shipping information</h1>

<?php

        $error=false;


if(isset($_POST['billing']))
{
    $fullnameIN = trim($_POST['fullname']);
    $phoneIN = trim($_POST['phone']);
    $RegionIN = trim($_POST['region']);
    $cityIN = trim($_POST['city']);
    $zipcodeIN = trim($_POST['zipcode']);
    $streetIN = trim($_POST['street']);
    $houseIN = trim($_POST['number']);


        $idout = createBillingAddress($fullnameIN,$phoneIN,$houseIN,$cityIN,$streetIN,$zipcodeIN,$RegionIN);
        $userid = getUserID($_COOKIE["LoggedinUser"]);
        setUserBillingAddress($userid,$idout);

        header('Location: index.php?page=profile');
        exit;

}

?>

<?php


        $username=$_COOKIE["LoggedinUser"];
        $id=getBillingAddressID($username);

    if($id!=null){
        $data = getBillingInfo($id);

        $fullnameIN = $data["@nameOUT"];
        $phoneIN = $data["@phoneOUT"];
        $RegionIN = $data["@regionOUT"];
        $cityIN = $data["@cityOUT"];
        $zipcodeIN = $data["@zipcodeOUT"];
        $streetIN = $data["@streetOUT"];
        $houseIN = $data["@numberOUT"];
    }


    echo'
  <form method="post" action="">
    <div class="shippingtop">


              <p><input class="fullname" type="text" name="fullname" id="inputProfileFullName" placeholder="Full Name" maxlength="254" value="'.$fullnameIN.'" required></p>

              <p><input class="phone" type="tel" name="phone" id="inputProfilePhone" placeholder="Phone Number" maxlength="9" value="'.$phoneIN.'" required></p>

    </div>

    <div class="shippingbottom">

              <p><input class="region" type="text" name="region" id="inputRegion" placeholder="Region" maxlength="50" value="'.$RegionIN.'" required></p>
              <p><input class="city" type="text" name="city" id="inputCity" placeholder="City" maxlength="50" value="'.$cityIN.'" required></p>
              <p><input class="zipcode" type="number" name="zipcode" id="inputZipCode" placeholder="ZipCode" maxlength="4" value="'.$zipcodeIN.'" required></p>
              <p><input class="street" type="text" name="street" id="inputStreet" placeholder="Street" maxlength="254" value="'.$streetIN.'" required></p>
              <p><input class="number" type="text" name="number" id="inputnumber" placeholder="Number" maxlength="254" value="'.$houseIN.'" required></p>

    </div>
              <p><input class="buttonSaveShipping productBtn2"  type="submit" name="billing" value="Save"></p>
          </form>';

?>
</section>

<section id="profilepassword">

  <h1 class="redtitle">Change password</h1>

<?php

    if(isset($_POST['sendMessage']))
{
    $valUserName = trim($_POST['username']);

    $valOldPass = sha1(trim($_POST['password']));
    $valPass1 = sha1(trim($_POST['password1']));
    $valPass2 = sha1(trim($_POST['password2']));


    $error = false;
    $valUserName = $_COOKIE["LoggedinUser"];
    $usercheck=getUserPAss($valUserName);

    if($valOldPass!=$usercheck)
    {
     $error ="Rosz régi jelszó";
    }
    else if($valPass1!=$valPass2){
       $error ="Jelszavak nem egyezznek";
    }

    else
    {
        updateUserPasswd($valUserName,$valPass1);

        header('Location: index.php?page=logout');
        exit;
    }
}



?>

  <form method="post" action="">
              <p><input class="password" type="password" name="password" id="inputPasswordOld" placeholder="Old password" maxlength="254" value="" required></p>
              <p><input class="password" type="password" name="password1" id="inputPasswordNew" placeholder="New password" maxlength="254" value="" required></p>
              <p><input class="password" type="password" name="password2" id="inputPasswordNew" placeholder="New password again" maxlength="254" value="" required></p>
              <p><input class="buttonSaveShipping productBtn2"  type="submit" name="sendMessage" value="Save"></p>
          </form>

</section>

<?php

    if($error != null)
    {
        echo '<h2 style="color:red; text-align:center;">'. $error .'</h2>';
    }
?>

</div>
