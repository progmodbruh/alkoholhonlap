-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Gép: localhost
-- Létrehozás ideje: 2021. Máj 06. 11:19
-- Kiszolgáló verziója: 8.0.18
-- PHP verzió: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `alcoholwebshop`
--

DELIMITER $$
--
-- Eljárások
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `create_address` (IN `houseIN` INT, IN `cityIN` VARCHAR(45), IN `streetIN` VARCHAR(45), IN `zipcodeIN` INT, IN `RegionIN` VARCHAR(45), OUT `idOUT` INT)  NO SQL
BEGIN
CALL create_city(cityIN,zipcodeIN,RegionIN,@a);
CALL create_street(streetIN,@b);
INSERT INTO `address`(`address`.`house number`,`address`.`City_id`,`address`.`Street_id`) VALUES(houseIN,@a,@b);
SET idOUT = LAST_INSERT_ID();
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_billing_address` (IN `fullnameIN` VARCHAR(45), IN `phoneIN` VARCHAR(11), IN `houseIN` VARCHAR(45), IN `cityIN` VARCHAR(45), IN `streetIN` VARCHAR(45), IN `zipcodeIN` INT, IN `RegionIN` VARCHAR(45), OUT `idOUT` INT)  NO SQL
BEGIN
CALL create_address(houseIN,cityIN,streetIN,zipcodeIN,regionIN,@a);
INSERT INTO `billing address`(`billing address`.`full name`,`billing address`.`phone number`,`billing address`.`Address_id`) VALUES(fullnameIN,phoneIN,@a);
SET idOUT = LAST_INSERT_ID();
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_city` (IN `cityIN` VARCHAR(45), IN `zipcodeIN` INT, IN `regionIN` VARCHAR(45), OUT `idOUT` INT)  NO SQL
BEGIN
CALL create_zipcode(zipcodeIN,regionIN,@a);
  Select `city`.`id` INTO idOUT From `city` Where `city`.`city name` = cityIN;
  IF FOUND_ROWS() = 0 THEN
    Insert Into `city`(`city`.`city name`,`city`.`Zipcode_id`) Value (cityIN,@a);
    SET idOUT = LAST_INSERT_ID();
  END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_order` (IN `billingIN` INT, IN `paymentIN` INT, OUT `idOUT` INT)  NO SQL
BEGIN
INSERT INTO `order`(`order`.`Billing address_id`,`order`.`Payment_id`) VALUES(billingIN,paymentIN);
SET idOUT = LAST_INSERT_ID();
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_rating` (IN `nameIN` VARCHAR(1000), IN `ratingIN` INT, IN `commentIN` TEXT, IN `ProductidIN` INT)  NO SQL
INSERT INTO `rating`(`rating`.`name`,`rating`.`rating`,`rating`.`comment`,`rating`.`Product_id`) VALUES(nameIN,ratingIN,commentIN,ProductidIN)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_street` (IN `streetIN` VARCHAR(45), OUT `idOUT` INT)  NO SQL
BEGIN
  Select `street`.`id` INTO idOUT From `street` Where `street`.`street name` = streetIN;
  IF FOUND_ROWS() = 0 THEN
    Insert Into `street`(`street`.`street name`) Value (streetIN);
    SET idOUT = LAST_INSERT_ID();
  END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_user` (IN `usernameIN` VARCHAR(1000), IN `passwordIN` VARCHAR(1000), IN `emailIN` VARCHAR(264), IN `birthdateIN` DATE)  NO SQL
INSERT INTO `user`(`user`.`username`,`user`.`password`,`user`.`e-mail`,`user`.`birthdate`) VALUES (usernameIN,passwordIN,emailIN,birthdateIN)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_zipcode` (IN `zipcodeIN` INT, IN `RegionIN` VARCHAR(45), OUT `idOUT` INT)  NO SQL
BEGIN
CALL get_region_id(RegionIN,@a);
  Select `zipcode`.`id` INTO idOUT From `zipcode` Where `zipcode`.`zipcode` = zipcodeIN;
  IF FOUND_ROWS() = 0 THEN
    Insert Into `zipcode`(`zipcode`.`zipcode`,`zipcode`.`Region_id`) Value (zipcodeIN,@a);
    SET idOUT = LAST_INSERT_ID();
  END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_bill_info` (IN `billidIN` INT, OUT `nameOUT` VARCHAR(1000), OUT `phoneOUT` VARCHAR(1000), OUT `regionOUT` VARCHAR(1000), OUT `cityOUT` VARCHAR(1000), OUT `zipcodeOUT` VARCHAR(1000), OUT `streetOUT` VARCHAR(1000), OUT `numberOUT` INT)  NO SQL
BEGIN 
SELECT `billing address`.`full name` INTO nameOUT FROM `billing address` WHERE `billing address`.`id`=billidIN;
SELECT `billing address`.`phone number` INTO phoneOUT FROM `billing address` WHERE `billing address`.`id`=billidIN;
SELECT `billing address`.`Address_id` INTO @a FROM `billing address` WHERE `billing address`.`id`=billidIN;
SELECT `address`.`house number` INTO numberOUT FROM `address` WHERE `address`.`id`=@a;
SELECT `address`.`City_id` into @c FROM `address` WHERE `address`.`id`=@a;
SELECT `address`.`Street_id` into @s FROM `address` WHERE `address`.`id`=@a;
SELECT `street`.`street name` INTO streetOUT FROM `street` WHERE `street`.`id`=@s;
SELECT `city`.`city name` INTO cityOUT FROM `city` WHERE `city`.`id`=@c;
SELECT `city`.`Zipcode_id` into @z FROM `city` WHERE `city`.`id`=@c;
SELECT `zipcode`.`zipcode` INTO zipcodeOUT FROM `zipcode` WHERE `zipcode`.`id`=@z;
SELECT `zipcode`.`Region_id` into @r FROM `zipcode` WHERE `zipcode`.`id`=@z;
SELECT `region`.`region name` INTO regionOUT FROM `region` WHERE `region`.`id`=@r;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_region_id` (IN `regionIN` VARCHAR(45), OUT `idOUT` INT)  NO SQL
SELECT `region`.`id` INTO idOUT FROM `region` WHERE `region`.`region name`=regionIN$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_billing_address_id` (IN `nameIN` VARCHAR(45), OUT `idOUT` INT)  NO SQL
SELECT `user`.`Billing address_id` into idOUT FROM `user` WHERE `user`.`username`=nameIN$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_birth` (IN `nameIN` VARCHAR(45), OUT `birthOUT` DATE)  NO SQL
SELECT `user`.`birthdate` INTO birthOUT FROM `user` WHERE `user`.`username`=nameIN$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_email` (IN `nameIN` VARCHAR(45), OUT `emailOUT` VARCHAR(256))  NO SQL
BEGIN
SELECT `user`.`e-mail` INTO emailOUT FROM `user` WHERE `user`.`username`=nameIN;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_id_by_email` (IN `emailIN` VARCHAR(1000), OUT `idOUT` INT)  NO SQL
SELECT `user`.`id` into idOUT FROM `user` WHERE `user`.`e-mail`=emailIN$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_id_by_name` (IN `nameIN` VARCHAR(45), OUT `idOUT` INT)  NO SQL
SELECT `user`.`id` INTO idOUT FROM `user` WHERE  `user`.`username`=nameIN$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_pass_by_name` (IN `usernameIN` VARCHAR(1000), OUT `passwdOUT` VARCHAR(1000))  NO SQL
SELECT `user`.`password` INTO passwdOUT FROM `user` WHERE `user`.`username`=usernameIN$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `set_order_product` (IN `OrderidIN` INT, IN `ProductIN` INT, IN `Quantity` INT)  NO SQL
BEGIN
INSERT INTO `order_has_product` VALUES(OrderidIN,ProductIN,Quantity); 

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `set_user_billing_address` (IN `useridIN` INT, IN `billingIN` INT)  NO SQL
UPDATE `user` SET `user`.`Billing address_id`=billingIN WHERE `user`.`id`=useridIN$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_user_passwd` (IN `usernameIN` VARCHAR(1000), IN `passwdIN` VARCHAR(1000))  NO SQL
UPDATE `user` SET `user`.`password`=passwdIN WHERE `user`.`username`=usernameIN$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `address`
--

CREATE TABLE `address` (
  `id` int(11) NOT NULL,
  `house number` int(11) NOT NULL,
  `City_id` int(11) NOT NULL,
  `Street_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `address`
--

INSERT INTO `address` (`id`, `house number`, `City_id`, `Street_id`) VALUES
(1, 2, 1, 1),
(2, 2, 1, 1),
(3, 12, 1, 2),
(4, 12, 1, 2),
(5, 13, 1, 2),
(6, 13, 1, 2),
(7, 13, 1, 2),
(8, 13, 1, 2),
(9, 2, 1, 3);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `billing address`
--

CREATE TABLE `billing address` (
  `id` int(11) NOT NULL,
  `full name` varchar(45) NOT NULL,
  `phone number` char(11) NOT NULL,
  `Address_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `billing address`
--

INSERT INTO `billing address` (`id`, `full name`, `phone number`, `Address_id`) VALUES
(1, 'Daniel Solti', '307883961', 1),
(2, 'Daniel Solti', '307883961', 2),
(3, 'Daniel Solti', '307883961', 3),
(4, 'Daniel Solti', '307883961', 4),
(5, 'Daniel Solti', '307883961', 5),
(6, 'test', '306543425', 6),
(7, 'test', '306543425', 7),
(8, 'test', '306543425', 8),
(9, 'Daniel Solti', '301234567', 9);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `city name` varchar(45) NOT NULL,
  `Zipcode_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `city`
--

INSERT INTO `city` (`id`, `city name`, `Zipcode_id`) VALUES
(1, 'Cserkút', 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `contact`
--

CREATE TABLE `contact` (
  `name` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(254) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `topic` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- A tábla adatainak kiíratása `contact`
--

INSERT INTO `contact` (`name`, `email`, `topic`, `message`, `id`) VALUES
('dani', 'dani@dani.com', 'test', 'good very nice', 3);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `Billing address_id` int(11) NOT NULL,
  `Payment_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `order`
--

INSERT INTO `order` (`id`, `Billing address_id`, `Payment_id`) VALUES
(1, 3, 1),
(2, 4, 3),
(5, 5, 3),
(6, 6, 1),
(7, 7, 2),
(8, 8, 3);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `order_has_product`
--

CREATE TABLE `order_has_product` (
  `Order_id` int(11) NOT NULL,
  `Product_id` int(11) NOT NULL,
  `Quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `order_has_product`
--

INSERT INTO `order_has_product` (`Order_id`, `Product_id`, `Quantity`) VALUES
(1, 4, 1),
(1, 5, 1),
(2, 4, 1),
(2, 5, 1),
(5, 3, 1),
(5, 6, 1),
(5, 9, 1),
(6, 3, 1),
(6, 6, 1),
(6, 9, 1),
(7, 3, 1),
(7, 6, 1),
(7, 9, 1),
(8, 3, 1),
(8, 6, 1),
(8, 9, 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `payment`
--

CREATE TABLE `payment` (
  `id` int(11) NOT NULL,
  `type` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `payment`
--

INSERT INTO `payment` (`id`, `type`) VALUES
(1, 'Online bank card'),
(2, 'Cash on delivery'),
(3, 'Money transfer');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `product_name` varchar(45) NOT NULL,
  `picture` varchar(40) NOT NULL,
  `price` int(11) NOT NULL,
  `description` text NOT NULL,
  `volume` decimal(10,0) NOT NULL,
  `alcohol_content` decimal(10,0) NOT NULL,
  `Type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `product`
--

INSERT INTO `product` (`id`, `product_name`, `picture`, `price`, `description`, `volume`, `alcohol_content`, `Type_id`) VALUES
(1, 'Világos Búza', 'vilagosbuza.png', 750, 'Világos búza Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eleifend diam eget leo sagittis congue. Nunc purus lectus, congue vitae placerat dictum, suscipit a velit.', '5', '3', 1),
(2, 'Cserkúti meggy', 'cserkutimeggy.png', 750, 'Cserkúti meggy Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras sit amet neque dictum, mattis ipsum aliquet, fermentum mi. In metus odio, tincidunt eu iaculis quis, dignissim nec eros.', '5', '3', 1),
(3, 'Áfonya ipa', 'afonyaipa.png', 750, 'Áfonya ipa Duis laoreet, diam a tempus interdum, nulla dui sodales mauris, non iaculis ligula odio eu nisl. Fusce eros ex, consequat ac imperdiet quis, viverra ultrices ex.', '5', '3', 1),
(4, 'Zöldcitrom keserű', 'zoldcitromkeseru.png', 750, 'Zöldcitrom keserű Nullam eget imperdiet sapien. Morbi feugiat dignissim tellus, eu consequat ligula tincidunt dictum.', '5', '3', 1),
(5, 'Egri bikavér', 'bikaver.png', 1490, 'Egri bikavér Nulla id justo tincidunt, porta massa quis, aliquet elit. Sed dignissim, lectus sit amet luctus blandit, enim justo fringilla tortor, eget auctor odio velit ac metus.', '7', '12', 2),
(6, 'Tokaji aszú 5 puttonyos', 'tokajiaszu.png', 5200, 'Tokaji aszú Duis massa est, interdum sit amet nisi consequat, malesuada efficitur ipsum. Cras nec ipsum nec eros tempor dapibus.', '5', '12', 2),
(7, 'Irsai Olivér', 'irsaioliver.png', 1300, 'Irsai Olivér Duis interdum tellus sagittis sapien consequat finibus eu eu magna.', '7', '12', 2),
(8, 'Jack Daniels', 'jackdaniels.png', 6950, 'Jack Daniels Integer luctus massa rutrum nulla fringilla, ac porttitor ligula feugiat.', '7', '40', 3),
(9, 'Jim Beam', 'jimbeam.png', 5400, 'Jim Beam Aenean at lacus erat. Phasellus vitae imperdiet ligula.', '7', '40', 3),
(10, 'Ballantine\'s', 'ballantines.png', 4990, 'Ballantines Etiam a sagittis diam. Quisque rhoncus turpis a tellus bibendum, at vestibulum mauris fermentum.', '7', '40', 3),
(11, 'Captain Morgan Spiced Gold', 'cptnmorgan.png', 4990, 'Captain Morgan Vivamus lorem nibh, posuere ut vehicula in, vehicula sit amet nibh.', '7', '35', 4),
(12, 'Deadhead ', 'deadhead.png', 18000, 'Deadhead Aenean a suscipit quam. Nunc nunc velit, efficitur at est nec, laoreet ornare urna.', '7', '40', 4),
(13, 'Myers\'s ', 'myers.png', 6500, 'Myers\'s Nunc ipsum velit, consectetur sit amet rhoncus eget, tristique id lectus. Morbi ac luctus mi.', '7', '40', 4),
(14, 'Fütyülős Mézes Barack', 'futyulos.png', 3100, 'Fütyülős Proin erat dolor, euismod et felis at, porttitor aliquet dui.', '5', '35', 5),
(15, 'Zimek Szilva Strong', 'zimek.png', 8700, 'Zimek Nullam elit quam, finibus sed hendrerit a, auctor ac enim.', '5', '55', 5),
(16, 'Agárdi Prémium Kökény', 'agardikokeny.png', 8300, 'Agárdi Prémium Kökény Duis a fringilla lacus. Maecenas aliquet nulla vel ipsum laoreet laoreet.', '4', '40', 5),
(17, 'Smirnoff Red', 'smirnoff.png', 6500, 'Smirnoff Pellentesque sed congue magna.', '10', '37', 6),
(18, 'Kalinka', 'kalinka.png', 2890, 'Kalinka Morbi eu egestas dolor. Pellentesque malesuada tellus et eros posuere mattis.', '5', '37', 6),
(19, 'Royal', 'royal.png', 2070, 'Royal Curabitur purus dolor, maximus vitae quam id, tincidunt interdum massa.', '5', '37', 6);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `rating`
--

CREATE TABLE `rating` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `rating` int(11) NOT NULL,
  `comment` text NOT NULL,
  `Product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `rating`
--

INSERT INTO `rating` (`id`, `name`, `rating`, `comment`, `Product_id`) VALUES
(1, 'solti', 3, 'franko', 1),
(2, 'solti', 4, '34tg34t34t', 1),
(3, '32r23r', 4, '123e13e12', 2),
(4, 'Dani', 5, 'super', 6),
(5, 'Horváth Charlie', 5, 'Jég dupla Whiskyvel \r\nKét dózis egy helyen \r\nEgy a társaság miatt, igen\r\nEgy, hogy jó napom legyen', 8),
(6, 'Jack Sparrow kapitány', 5, 'DE A RUMOT MIÉRT?', 11),
(7, 'The Shape', 5, 'It\'s me, Michael *tilts head to the right*', 13),
(8, 'Deadpool', 3, 'Perma drága miafene', 12),
(9, 'Yours truly', 1, 'no', 10),
(10, 'Rasputin', 4, 'Выходила на берег Катюша,\r\n\r\nНа высокий берег на крутой.', 18),
(11, 'A Legenda', 3, 'Húha, a k**va szádat, hány fokos?', 15),
(12, 'Pepsi Béla', 4, 'Ameddig tart a bor, addig megyek', 7),
(13, 'Theirs truly', 5, 'yes', 10),
(14, 'Joseph', 4, 'Decent quality.', 6),
(15, 'Michael', 5, 'Absolutely loved it.', 2),
(16, 'Chad', 3, 'Too bitter.', 1),
(17, 'San', 3, 'Too dry.', 1),
(18, 'Igor', 4, 'Good.', 19);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `region`
--

CREATE TABLE `region` (
  `id` int(11) NOT NULL,
  `region name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `region`
--

INSERT INTO `region` (`id`, `region name`) VALUES
(1, 'Baranya'),
(2, 'Zala'),
(3, 'Somogy'),
(4, 'Békés'),
(5, 'Vas '),
(6, 'Nógrád'),
(7, 'Fejér'),
(8, 'Csongrád-Csanád'),
(10, 'Győr-Moson-Sopron'),
(11, 'Heves'),
(12, 'Jász-Nagykun-Szolnok'),
(13, 'Hajdú-Bihar'),
(14, 'Pest'),
(15, 'Tolna'),
(16, 'Komárom-Esztergom'),
(17, 'Bács-Kiskun'),
(18, 'Borsod-Abaúj-Zemplén'),
(19, 'Szabolcs-Szatmár-Bereg'),
(20, 'Veszprém');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `street`
--

CREATE TABLE `street` (
  `id` int(11) NOT NULL,
  `street name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `street`
--

INSERT INTO `street` (`id`, `street name`) VALUES
(1, 'József Attila utca 15.'),
(2, 'József Attila utca'),
(3, 'Test');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `type`
--

CREATE TABLE `type` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `type`
--

INSERT INTO `type` (`id`, `name`) VALUES
(1, 'Sör'),
(2, 'Bor'),
(3, 'Whiskey'),
(4, 'Rum'),
(5, 'Pálinka'),
(6, 'Vodka');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(1000) NOT NULL,
  `e-mail` varchar(254) NOT NULL,
  `birthdate` date NOT NULL,
  `Billing address_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `e-mail`, `birthdate`, `Billing address_id`) VALUES
(1, 'dani', '3f547476e0ce8e681ded188f0322a4d5e0d56ec4', 'solti.dani7@gmail.com', '2000-12-29', 9);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `zipcode`
--

CREATE TABLE `zipcode` (
  `id` int(11) NOT NULL,
  `zipcode` char(4) NOT NULL,
  `Region_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `zipcode`
--

INSERT INTO `zipcode` (`id`, `zipcode`, `Region_id`) VALUES
(1, '7673', 1),
(2, '7663', 1);

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`,`City_id`,`Street_id`),
  ADD KEY `fk_Address_City1_idx` (`City_id`),
  ADD KEY `fk_Address_Street1_idx` (`Street_id`);

--
-- A tábla indexei `billing address`
--
ALTER TABLE `billing address`
  ADD PRIMARY KEY (`id`,`Address_id`),
  ADD KEY `fk_Billing address_Address1_idx` (`Address_id`);

--
-- A tábla indexei `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`,`Zipcode_id`),
  ADD KEY `fk_City_Zipcode1_idx` (`Zipcode_id`);

--
-- A tábla indexei `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`,`Billing address_id`,`Payment_id`),
  ADD KEY `fk_Order_Billing address1_idx` (`Billing address_id`),
  ADD KEY `fk_Order_Payment1_idx` (`Payment_id`);

--
-- A tábla indexei `order_has_product`
--
ALTER TABLE `order_has_product`
  ADD PRIMARY KEY (`Order_id`,`Product_id`),
  ADD KEY `fk_Order_has_Product_Product1_idx` (`Product_id`),
  ADD KEY `fk_Order_has_Product_Order1_idx` (`Order_id`);

--
-- A tábla indexei `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`,`Type_id`),
  ADD KEY `fk_Product_Type1_idx` (`Type_id`);

--
-- A tábla indexei `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`,`Product_id`),
  ADD KEY `fk_Rating_Product1_idx` (`Product_id`);

--
-- A tábla indexei `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `street`
--
ALTER TABLE `street`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_User_Billing address1_idx` (`Billing address_id`);

--
-- A tábla indexei `zipcode`
--
ALTER TABLE `zipcode`
  ADD PRIMARY KEY (`id`,`Region_id`),
  ADD KEY `fk_Zipcode_Region_idx` (`Region_id`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `address`
--
ALTER TABLE `address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT a táblához `billing address`
--
ALTER TABLE `billing address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT a táblához `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT a táblához `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT a táblához `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT a táblához `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT a táblához `rating`
--
ALTER TABLE `rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT a táblához `region`
--
ALTER TABLE `region`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT a táblához `street`
--
ALTER TABLE `street`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT a táblához `type`
--
ALTER TABLE `type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT a táblához `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `zipcode`
--
ALTER TABLE `zipcode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Megkötések a kiírt táblákhoz
--

--
-- Megkötések a táblához `address`
--
ALTER TABLE `address`
  ADD CONSTRAINT `fk_Address_City1` FOREIGN KEY (`City_id`) REFERENCES `city` (`id`),
  ADD CONSTRAINT `fk_Address_Street1` FOREIGN KEY (`Street_id`) REFERENCES `street` (`id`);

--
-- Megkötések a táblához `billing address`
--
ALTER TABLE `billing address`
  ADD CONSTRAINT `fk_Billing address_Address1` FOREIGN KEY (`Address_id`) REFERENCES `address` (`id`);

--
-- Megkötések a táblához `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `fk_City_Zipcode1` FOREIGN KEY (`Zipcode_id`) REFERENCES `zipcode` (`id`);

--
-- Megkötések a táblához `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `fk_Order_Billing address1` FOREIGN KEY (`Billing address_id`) REFERENCES `billing address` (`id`),
  ADD CONSTRAINT `fk_Order_Payment1` FOREIGN KEY (`Payment_id`) REFERENCES `payment` (`id`);

--
-- Megkötések a táblához `order_has_product`
--
ALTER TABLE `order_has_product`
  ADD CONSTRAINT `fk_Order_has_Product_Order1` FOREIGN KEY (`Order_id`) REFERENCES `order` (`id`),
  ADD CONSTRAINT `fk_Order_has_Product_Product1` FOREIGN KEY (`Product_id`) REFERENCES `product` (`id`);

--
-- Megkötések a táblához `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk_Product_Type1` FOREIGN KEY (`Type_id`) REFERENCES `type` (`id`);

--
-- Megkötések a táblához `rating`
--
ALTER TABLE `rating`
  ADD CONSTRAINT `fk_Rating_Product1` FOREIGN KEY (`Product_id`) REFERENCES `product` (`id`);

--
-- Megkötések a táblához `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_User_Billing address1` FOREIGN KEY (`Billing address_id`) REFERENCES `billing address` (`id`);

--
-- Megkötések a táblához `zipcode`
--
ALTER TABLE `zipcode`
  ADD CONSTRAINT `fk_Zipcode_Region` FOREIGN KEY (`Region_id`) REFERENCES `region` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
